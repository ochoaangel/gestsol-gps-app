import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Router } from '@angular/router';
import { HttpHeaders } from '@angular/common/http'
import { Observable, Subscriber } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MyserviceService {

  access_token: string;
  urlpost = "https://socketgpsv1.gestsol.cl/api/gestsol-auth";
  urlget = "https://socketgpsv1.gestsol.cl/sapi/devices";

  constructor(private httpClient: HttpClient, ) { }

  getsolAuth(user: any): Observable<any> {
    return new Observable((observer: Subscriber<any>) => {
      this.httpClient.post<any>(this.urlpost, user).subscribe(dataAPI => {
        if (dataAPI) {
          this.access_token = dataAPI.access_token;
          observer.next(dataAPI);
          observer.complete();
        } else {
          observer.next("");
          observer.complete();
        }
      });
    });
  }

  getDevices(): Observable<any> {
    return new Observable((observer: Subscriber<any>) => {
      const headers = new HttpHeaders().set("Authorization", "Bearer " + this.access_token);
      this.httpClient.get<any>(this.urlget, { headers }).subscribe(dataAPI => {
        observer.next(dataAPI);
        observer.complete();
      });
    });
  }

}
