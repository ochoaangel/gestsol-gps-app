import { Component, OnInit } from '@angular/core';
import { MyserviceService } from 'src/app/services/myservice.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  mydata = { username: "mauricio.fuentes@pullmancosta.cl", password: "CostaGps+2019" }

  constructor(private mys: MyserviceService, private router: Router) { }

  ngOnInit() { }

  enviar() {
    this.mys.getsolAuth(this.mydata).subscribe(result => {
      if (result && result.access_token) {
        this.router.navigateByUrl('/user-panel');
      } else if(result && result.error ){
        alert(result.error_description);
      }
    })
  }

}
