import { Component, OnInit } from '@angular/core';
import { MyserviceService } from 'src/app/services/myservice.service';



@Component({
  selector: 'app-user-panel',
  templateUrl: './user-panel.page.html',
  styleUrls: ['./user-panel.page.scss'],
})
export class UserPanelPage implements OnInit {

  constructor(private mys: MyserviceService) { }

  data: any;
  ngOnInit() {
    this.mys.getDevices().subscribe(result => {
      this.data = result.data;
    })
  }

}
